import { ObjectId } from 'promised-mongo';

const { AuthenticationError, ForbiddenError } = require('apollo-server');

const { query } = require('nact');

const resource = 'post' || 'proposal';

module.exports = {

  Mutation: {
    changeMyPost: async (obj, args, ctx, info) => {
      let user = await ctx.user;

      if (!user) throw new AuthenticationError('must authenticate');

      if (!ctx.access.can(user.role).updateOwn(resource).granted) throw new ForbiddenError('Forbidden');

      let post;
      if (args.id) {
        post = await ctx.db.post.findAndModify({
          query: { _id: new ObjectId(args.id) },
          update: {
            $set: {
              title: args.title,
              text: args.text,
            },
          },
        });
      } else {
        post = await ctx.db.post.insert({
          title: args.title,
          text: args.text,
          date: new Date(),
          author_id: new ObjectId(user.id),
        });
      }

      return post;
    },
    changeProposal: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      if (args.input.circle_id) {
        args.input.circle_id = new ObjectId(args.input.circle_id);
      }

      if (args.id) {
        return await query(collectionItemActor, {
          type: 'proposal',
          search: { _id: args.id },
          input: args.input,
        }, global.actor_timeout);
      }
      return await query(collectionItemActor, { type: 'proposal', input: args.input }, global.actor_timeout);
    },
    changeMyProposal: async (obj, args, ctx, info) => {
      let user = await ctx.user;

      const collectionItemActor = ctx.children.get('item');

      if (args.input.circle_id) {
        args.input.circle_id = new ObjectId(args.input.circle_id);
      }

      if (args.id) {
        // добавить условия про my args.input.author_id = new ObjectId(user.id);
        return await query(collectionItemActor, {
          type: 'proposal',
          search: { _id: args.id },
          input: args.input,
        }, global.actor_timeout);
      }
      args.input.date = new Date();
      args.input.author_id = new ObjectId(user._id);
      return await query(collectionItemActor, { type: 'proposal', input: args.input }, global.actor_timeout);
    },
    voteProposal: async (obj, args, ctx, info) => {
      let vote = await ctx.db.vote.insert({
        proposal_id: args.proposal_id,
        type: args.type,
        author_id: ctx.user._id,
        date: new Date(),
      });

      let proposal = await ctx.db.proposal.findAndModify({
        query: { _id: new ObjectId(args.proposal_id) },
        update: {
          $push: { votes_ids: new ObjectId(vote._id) },
        },
      });

      return proposal;
    },

  },
  Query: {
    getPost: async (obj, args, ctx, info) => {
      let user = await ctx.user;

      if (!user) throw new AuthenticationError('must authenticate');

      if (!ctx.access.can(user.role).readAny(resource).granted) throw new ForbiddenError('Forbidden');

      let posts = await ctx.db.post.aggregate(
        [
          { $match: { _id: new ObjectId(args.id) } },
          {
            $lookup: {
              from: 'user',
              localField: 'author_id',
              foreignField: '_id',
              as: 'author',
            },
          },
          { $limit: 1 },
        ],
      );

      let post = posts[0];
      return post || {};
    },

    getPosts: async (obj, args, ctx, info) => {
      let user = await ctx.user;

      if (!user) throw new AuthenticationError('must authenticate');

      if (!ctx.access.can(user.role).readAny(resource).granted) throw new ForbiddenError('Forbidden');

      return await ctx.db.post.aggregate(
        [
          { $match: { author_id: new ObjectId(args.author_id) } },
          { $sort: { date: -1 } },
          {
            $lookup: {
              from: 'user',
              localField: 'author_id',
              foreignField: '_id',
              as: 'author',
            },
          },
          { $unwind: '$author' },
        ],
      );
    },

    getProposal: async (obj, args, ctx, info) => {
      let user = await ctx.user;

      let proposals = await ctx.db.proposal.aggregate(
        [
          { $match: { _id: new ObjectId(args.id) } },
          { $sort: { date: -1 } },

        ],
      );

      return proposals[0];
    },

    getProposals: async (obj, args, ctx, info) => {
      let user = await ctx.user;

      let proposals = await ctx.db.proposal.aggregate(
        [
          { $sort: { date: -1 } },

        ],
      );

      return proposals;
    },
  },
  Proposal: {
    author: async (obj, args, ctx, info) => (await ctx.db.user.find({ _id: new ObjectId(obj.author_id) }))[0],
    circle: async (obj, args, ctx, info) => (await ctx.db.circle.find({ _id: new ObjectId(obj.circle_id) }))[0],
    votes: async (obj, args, ctx, info) => {
      if (obj.votes_ids) {
        return await ctx.db.vote.findAsCursor({ _id: { $in: obj.votes_ids } }).sort({ date: -1 }).toArray();
      }
    },
  },
  Vote: {
    author: async (obj, args, ctx, info) => (await ctx.db.user.find({ _id: new ObjectId(obj.author_id) }))[0],
  },
};
